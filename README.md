# Tokens de Design

Este é o repositório de Tokens para os projetos [GOVBR-DS](http://gov.br/ds/).

## Tecnologias

Esse projeto é desenvolvido usando:

- [Node.js](https://nodejs.org/)
- [Style Dictionary](https://amzn.github.io/style-dictionary/#/)

## Como usar nossa biblioteca em seu projeto?

### Instalação dos tokens

- Baixe o pacote de tokens usando o comando no terminal: `npm install --save @govbr-ds/tokens`
- Dentro da pasta "dist" do pacote estão disponíveis os formatos CSS, SCSS, JS e JSON. Carregue o formato adequado para a sua solução web

Exemplo em CSS:

```html
<head>
  <link rel="stylesheet" href="endereço-dos-tokens/dist/css/tokens.css" />
  <link rel="stylesheet" href="endereço-dos-tokens/dist/css/themes/light.css" />
  <link rel="stylesheet" href="endereço-dos-tokens/dist/css/themes/dark.css" />
</head>
```

Exemplo em SCSS:

```scss
// Carregar tokens
@use 'endereço-dos-tokens/dist/scss/tokens' as tokens;
@use 'endereço-dos-tokens/dist/scss/themes/light' as light;
@use 'endereço-dos-tokens/dist/scss/themes/dark' as dark;
```

Mais informações sobre `@use` em <https://sass-lang.com/documentation/at-rules/use/>.

### Uso dos tokens

### CSS

Os tokens começando com o prefixo `--theme` são modificáveis por tema, enquanto que os restantes possuem valores fixos.

```css
/* Exemplo sem tema */
.seletor {
  background-color: var(--color-red-50);
  color: var(--color-pure-100);
  margin: var(--spacing-layout-03);
  padding: var(--spacing-adjust-01);
}

/* Exemplo com tema */
.seletor {
  background-color: var(--theme-interactive-main);
  color: var(--theme-interactive-text-main);
}
```

Use o atributo `[data-br-theme="light"]` ou `[data-br-theme="dark"]` para mudar os tokens de tema.

```html
<!-- Aplica tema "light" -->
<div data-br-theme="light">
  <div class="seletor">...</div>
</div>

<!-- Aplica tema "dark" -->
<div data-br-theme="dark">
  <div class="seletor">...</div>
</div>
```

Para aplicar o tema de forma global use o atributo `data-br-theme` na tag de maior hierarquia da sua solução web, tal como `<body>` ou outra.

### SCSS

**Atenção!** Recomendamos usar os tokens no formato CSS para que os temas funcionem corretamente na sua solução web.

Os tokens no SCSS estão no formato **Map**. Para mais informações de uso, leia a documentação em <https://sass-lang.com/documentation/values/maps/>.

Exemplo para gerar um CSS com o valor original do token:

```scss
// Carregar tokens
@use 'endereço-dos-tokens/dist/scss/tokens' as tokens;
@use 'endereço-dos-tokens/dist/scss/themes/light' as light;
@use 'endereço-dos-tokens/dist/scss/themes/dark' as dark;

// Função para gerar token no formato var(--nome-token, valor)
@function token($token, $map: tokens.$tokens) {
  // Remove "--" do token
  $key: str-slice($token, $start-at: 3, $end-at: -1);

  // Acessa o valor no mapa de tokens
  $value: map-get($map, $key);

  @return var($token, $value);
}

/* Exemplo sem tema */
.seletor {
  background-color: token(--color-red-50);
  color: token(--color-pure-100);
  margin: token(--spacing-layout-03);
  padding: token(--spacing-adjust-01);
}

/* Exemplo com tema "dark" */
.seletor {
  background-color: token(--theme-interactive-main, dark.$tokens-dark);
  color: token(--theme-interactive-text-main, dark.$tokens-dark);
}
```

Irá gerar o seguinte CSS:

```css
/* Exemplo sem tema */
.seletor {
  background-color: var(--color-red-50, #d83933);
  color: var(--color-pure-100, #000000);
  margin: var(--spacing-layout-03, 24px);
  padding: var(--spacing-adjust-01, 4px);
}

/* Exemplo com tema "dark" */
.seletor {
  background-color: var(--theme-interactive-main, #81aefc);
  color: var(--theme-interactive-text-main, #000000);
}
```

### Customização dos tokens

1. Faça o clone deste repositório usando `git clone git@gitlab.com:govbr-ds/ds/govbr-ds-tokens.git`
2. Baixe todas as dependências do projeto usando `npm install`
3. Localize os tokens na pasta 'src' e faça as modificações necessárias
4. Compile os novos tokens usando `npm run build`

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GovBR-DS <http://gov.br/ds>

- Pelo nosso email <govbr-ds@serpro.gov.br>

- Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

## Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Apêndice

## Créditos

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') e [Dataprev](https://www.dataprev.gov.br/ 'Dataprev | Empresa de Tecnologia e Informações da Previdência') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
