const StyleDictionary = require('style-dictionary')

// Tokens de configuração
function config(path) {
  return {
    source: ['src/tokens/*.json', 'src/tokens/themes/light.json'],
    platforms: {
      scss: {
        transforms: StyleDictionary.transformGroup['scss'].concat(['content/quote', 'content/colorAlpha']),
        buildPath: `${path}/scss/`,
        files: [
          {
            destination: '_tokens.scss',
            filter: (token) => token.attributes.category !== 'configs',
            format: 'scss/map-flat',
            options: {
              themeable: true,
              showFileHeader: false,
            },
          },
        ],
      },
      css: {
        transforms: StyleDictionary.transformGroup['css'].concat(['content/quote', 'content/colorAlpha']),
        buildPath: `${path}/css/`,
        files: [
          {
            destination: 'tokens.css',
            format: 'css/variables',
            filter: (token) => token.attributes.category !== 'configs',
          },
        ],
      },
      js: {
        transforms: StyleDictionary.transformGroup['js'].concat(['name/cti/camel', 'content/colorAlpha']),
        buildPath: `${path}/js/`,
        files: [
          {
            destination: 'tokens.js',
            filter: (token) => token.attributes.category !== 'configs',
            format: 'javascript/es6',
            options: {
              showFileHeader: false,
            },
          },
        ],
      },
      json: {
        transforms: StyleDictionary.transformGroup['js'].concat(['name/cti/camel', 'content/colorAlpha']),
        buildPath: `${path}/json/`,
        files: [
          {
            destination: 'tokens.json',
            filter: (token) => token.attributes.category !== 'configs',
            format: 'json',
          },
        ],
      },
    },
  }
}

// Tokens de tema
function themesConfig(theme, path) {
  return {
    source: ['src/tokens/*.json', `src/tokens/themes/${theme}.json`],
    platforms: {
      scss: {
        transforms: [
          'attribute/cti',
          'name/cti/kebab',
          'time/seconds',
          'content/icon',
          'size/rem',
          'color/css',
          'content/quote',
          'content/colorAlpha',
        ],
        buildPath: `${path}/scss/themes/`,
        files: [
          {
            destination: `_${theme}.scss`,
            format: 'scss/map-flat',
            mapName: `tokens-${theme}`,
            filter: (token) => token.attributes.category === 'theme',
            options: {
              showFileHeader: false,
            },
          },
        ],
      },
      css: {
        transforms: [
          'attribute/cti',
          'name/cti/kebab',
          'time/seconds',
          'content/icon',
          'size/rem',
          'color/css',
          'content/quote',
          'content/colorAlpha',
        ],
        buildPath: `${path}/css/themes/`,
        files: [
          {
            destination: `${theme}.css`,
            format: 'css/variables',
            filter: (token) => token.attributes.category === 'theme',
            options: {
              selector: `[data-br-theme="${theme}"]`,
            },
          },
        ],
      },
      js: {
        transforms: ['attribute/cti', 'name/cti/camel', 'size/rem', 'color/hex', 'content/quote', 'content/colorAlpha'],
        buildPath: `${path}/js/themes/`,
        files: [
          {
            destination: `${theme}.js`,
            format: 'javascript/es6',
            filter: (token) => token.attributes.category === 'theme',
            options: {
              showFileHeader: false,
            },
          },
        ],
      },
      json: {
        transforms: ['attribute/cti', 'name/cti/camel', 'size/rem', 'color/hex', 'content/quote', 'content/colorAlpha'],
        buildPath: `${path}/json/themes/`,
        files: [
          {
            destination: `${theme}.json`,
            format: 'json',
            filter: (token) => token.attributes.category === 'theme',
          },
        ],
      },
    },
  }
}

// Adiciona aspas no tokens de CSS e SCSS
StyleDictionary.registerTransform({
  name: 'content/quote',
  type: `value`,
  transitive: true,
  matcher: function (token) {
    return token.type === 'fontFamilies'
  },
  transformer: (token) => {
    return `"${token.value}"`
  },
})

// Transforma cores com transparência
StyleDictionary.registerTransform({
  name: 'content/colorAlpha',
  type: `value`,
  transitive: true,
  matcher: function (token) {
    return token.alpha
  },
  transformer: (token) => {
    const colorOriginal = token.color

    // Remove qualquer caractere não válido
    const colorFiltered = colorOriginal.replace(
      /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => '#' + r + r + g + g + b + b
    )

    // Converte para um valor inteiro
    var colorInt = parseInt(colorFiltered.slice(1), 16)

    // Extração dos componentes RGB
    var r = (colorInt >> 16) & 255
    var g = (colorInt >> 8) & 255
    var b = colorInt & 255

    // Calculo do alpha
    const alpha = eval?.(`"use strict";(${token.alpha})`)

    return `rgba(${r}, ${g}, ${b}, ${alpha})`
  },
})

// Compila tokens de configuração
;['dist'].map(function (path) {
  console.log('\n==============================================')
  console.log('\nCompilar tokens no path: ' + path)
  StyleDictionary.extend(config(path)).buildAllPlatforms()

  // Compila tokens de tema
  ;['light', 'dark'].map(function (theme) {
    console.log('\n==============================================')
    console.log('\nCompilar tokens do tema: ' + theme)
    StyleDictionary.extend(themesConfig(theme, path)).buildAllPlatforms()
  })
})
